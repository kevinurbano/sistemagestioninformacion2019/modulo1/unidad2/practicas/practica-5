﻿USE practica5;

/* Consultas Practica 5 */

/* Consulta 1 */
SELECT 
  m.nombre_m
  FROM 
    composicion c
  JOIN
    miembro m USING(dni)
  WHERE 
    c.cargo='PRESIDENTE';

-- Optimizada
SELECT 
  c2.nombre_m
  FROM 
    (
      -- C1
      SELECT 
        DISTINCT dni 
        FROM 
          composicion c 
        WHERE 
          c.cargo='PRESIDENTE'
    ) c1
  JOIN
    (
      -- C2
      SELECT 
        dni,m.nombre_m 
        FROM 
          miembro m
    ) c2 USING(dni);

/* Consulta 2 */
SELECT 
  f.direccion
  FROM 
    composicion c 
  JOIN 
    federacion f USING(nombre)
  WHERE
    c.cargo='GERENTE';

-- Optimizada
SELECT 
  c2.direccion
  FROM
    (
      -- C1
      SELECT 
        DISTINCT c.nombre 
        FROM 
          composicion c 
        WHERE 
          c.cargo='GERENTE'
    ) c1
  JOIN
    (
      -- C2
      SELECT 
        f.nombre,f.direccion 
        FROM 
          federacion f
    ) c2 USING(nombre); 

/* Consulta 3 */
SELECT 
  c1.nombre
  FROM 
    (
      SELECT f.nombre FROM federacion f
    ) c1
  LEFT JOIN
    (
      SELECT DISTINCT c.nombre FROM composicion c WHERE c.cargo='ASESOR TECNICO'
    ) c2 USING(nombre) WHERE c2.nombre IS NULL;

/* Consulta 4 */
-- Numerador
SELECT 
  DISTINCT c.nombre,c.cargo -- Despues agrupamos por nombre de federacion para saber cuantos cargos tiene cada federacion
  FROM 
    composicion c;

-- Denominador
SELECT 
  DISTINCT c.cargo -- Despues tenemos que contar el numero de cargos que existen (sin repetidos)
  FROM 
    composicion c;

-- Division (federacion que tenga todos los cargos)
SELECT 
  nombre
  FROM
    (
      SELECT 
      DISTINCT c.nombre,c.cargo 
      FROM 
        composicion c
    ) c1
  GROUP BY 
    c1.nombre
  HAVING
    COUNT(*)=
      (
        SELECT 
          COUNT(DISTINCT c.cargo) 
        FROM 
          composicion c
      );

/* Consulta 5 */
SELECT
  c1.nombre
  FROM
    (SELECT 
      DISTINCT nombre 
      FROM 
        composicion c 
      WHERE
        c.cargo='Asesor Tecnico') c1
  NATURAL JOIN
    (SELECT 
      DISTINCT c.nombre 
      FROM 
        composicion c 
      WHERE 
        c.cargo='Psicologo') c2;

