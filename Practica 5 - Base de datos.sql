﻿DROP DATABASE IF EXISTS practica5;
CREATE DATABASE IF NOT EXISTS practica5;

USE practica5;

CREATE OR REPLACE TABLE federacion(
  nombre varchar(40),
  direccion varchar(50),
  telefono varchar(12),
  PRIMARY KEY(nombre)
  ); 

CREATE OR REPLACE TABLE miembro(
  dni varchar(11),
  nombre_m varchar(40),
  titulacion varchar(30),
  PRIMARY KEY(dni)
  );

CREATE OR REPLACE TABLE composicion(
  nombre varchar(40),
  dni varchar(11),
  cargo varchar(40),
  fecha_inicio date,
  PRIMARY KEY (nombre,dni)
  );

ALTER TABLE composicion 
  ADD 
    CONSTRAINT fkFederacionComposicion 
    FOREIGN KEY (nombre) REFERENCES federacion(nombre) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD
    CONSTRAINT fkFederacionMiembro
    FOREIGN KEY (dni) REFERENCES miembro(dni) ON DELETE CASCADE ON UPDATE CASCADE;

INSERT INTO federacion (nombre, direccion, telefono)
  VALUES ('federacion 1', 'direccion 1', '942111111'),
         ('federacion 2', 'direccion 2', '942222222'),
         ('federacion 3', 'direccion 3', '942333333');

INSERT INTO miembro (dni, nombre_m, titulacion)
  VALUES ('12345678A', 'miembro 1', 'titulacion 1'),
         ('12345678B', 'miembro 2', 'titulacion 2'),
         ('12345678C', 'miembro 3', 'titulacion 3');

INSERT INTO composicion (nombre, dni, cargo, fecha_inicio)
  VALUES ('federacion 1', '12345678A', 'PRESIDENTE', '2007/07/22'),
         ('federacion 1', '12345678B', 'GERENTE', '2010/03/12'),
         ('federacion 2', '12345678C', 'ASESOR TECNICO', '2004/08/27'),
         ('federacion 3', '12345678A', 'PSICOLOGO', '2004/08/27');

